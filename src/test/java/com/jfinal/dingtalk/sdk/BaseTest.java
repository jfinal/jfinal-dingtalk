package com.jfinal.dingtalk.sdk;

import com.jfinal.dingtalk.common.JFinalDingtalkConfig;
import com.jfinal.dingtalk.common.JFinalDingtalkKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import org.junit.Before;

/**
 * Created by L.cm on 2016/6/8.
 */
public abstract class BaseTest {

    @Before
    public void before() {
        Prop prop = PropKit.use("a_little_config.txt");

        JFinalDingtalkConfig config = new JFinalDingtalkConfig();
        config.setCorpId(prop.get("corpID"));
        config.setCorpSecret(prop.get("corpSecret"));
        config.setSsoSecret(prop.get("ssoSecret"));

        JFinalDingtalkKit.setThreadLocalApiConfig(config);
    }

}
