package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class MessageApi extends BaseApi {
    /**
     * 发送普通会话消息
     * @param data
     * @return
     */
    public ApiResult sendToConversation(Map<String, Object> data){
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.message.send_to_conversation", data);
    }

    /**
     * 发送企业消息接口
     * @param data
     * @return
     */
    public ApiResult send(Map<String, Object> data){
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.message.send", data);
    }

    /**
     * 获取会话
     * @param messageId	String	是	消息id
     * @return ApiResult
     */
    public ApiResult listMessageStatus(String messageId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("messageId", messageId)
                .getData();
        return post("api.message.list_message_status", data);
    }

}
