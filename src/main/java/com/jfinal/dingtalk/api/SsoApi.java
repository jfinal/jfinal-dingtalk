package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class SsoApi extends BaseApi {

    /**
     * 通过CODE换取微应用管理员的身份信息
     * @param code	String	是	通过Oauth认证会给URL带上CODE
     * @return ApiResult
     */
    public ApiResult getUserInfo(String code) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("code", code)
                .getData();
        return get("api.sso.getuserinfo", data);
    }

}
