package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * 微应用api
 * Created by L.cm on 2016/6/3.
 */
public class MicroAppApi extends BaseApi {

    /**
     * appIcon	String	是	微应用的图标。需要调用上传接口将图标上传到钉钉服务器后获取到的mediaId
     * appName	String	是	微应用的名称。长度限制为1~10个字符
     * appDesc	String	是	微应用的描述。长度限制为1~20个字符
     * homepageUrl	String	是	微应用的移动端主页，必须以http开头或https开头
     * pcHomepageUrl	String	否	微应用的PC端主页，必须以http开头或https开头，如果不为空则必须与homepageUrl的域名一致
     * ompLink	String	否	微应用的OA后台管理主页，必须以http开头或https开头
     */
    public ApiResult create(Map<String, Object> data){
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.microapp.create", data);
    }

    /**
     * 获取企业设置的微应用可见范围
     * @param agentId Long	是	需要查询询的微应用agentId
     * @return
     */
    public ApiResult getVisibleScopes(long agentId){
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("agentId", agentId)
                .getData();
        return post("api.microapp.visible_scopes", data);
    }

}
