package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * 文件api
 * Created by L.cm on 2016/6/3.
 */
public class FileApi extends BaseApi {
    /**
     * 预创建文件
     * @param size Long	是	文件大小
     * @return
     */
    public ApiResult createUpload(long size) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("size", size)
                .getData();
        return get("api.file.upload.create", data);
    }

    /**
     * // TODO 待完善：http://g.alicdn.com/dingding/opendoc/docs/_server/tab10-50.html?t=1466603208024
     *
     * @param uploadId String	是	上传文件唯一标识
     * @return
     */
    public ApiResult upload(String uploadId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("uploadid", uploadId)
                .getData();
        return post("api.file.upload", data);
    }


}
