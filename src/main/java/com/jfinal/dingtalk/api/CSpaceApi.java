package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class CSpaceApi extends BaseApi {

    /**
     * 发送文件给指定用户
     * @param agentId	String	是	文件发送者微应用的agentId
     * @param userId	String	是	文件接收人的userid
     * @param mediaId	String	是	调用钉盘上传文件接口得到的mediaid
     * @param fileName	String	是	文件名(需包含含扩展名)
     *
     * @return ApiResult
     */
    public ApiResult addToSingleChat(String agentId, String userId, String mediaId, String fileName) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("agent_id", agentId)
                .put("userid", userId)
                .put("media_id", mediaId)
                .put("file_name", fileName)
                .getData();
        return post("api.cspace.add_to_single_chat", data);
    }

    /**
     * 新增文件到用户钉盘
     *
     * @param code	String	是	用户授权码，访问用户数据时需要。参考客户端开发文档 - 容器
     * @param mediaId	String	是	调用钉盘上传文件接口得到的mediaid
     * @param spaceId	String	是	调用云盘选择控件后获取的用户钉盘空间ID
     * @param folderId	String	是	调用云盘选择控件后获取的用户钉盘文件夹ID
     * @param name	String	是	上传文件的名称，不能包含非法字符
     * @param overwrite	Boolean	否	遇到同名文件是否覆盖，若不覆盖，则会自动重命名本次新增的文件，默认为false
     *
     * @return ApiResult
     */
    public ApiResult add(String code, String mediaId, String spaceId, String folderId, String name, boolean overwrite) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("code", code)
                .put("media_id", mediaId)
                .put("space_id", spaceId)
                .put("folder_id", folderId)
                .put("name", name)
                .put("overwrite", overwrite)
                .getData();
        return get("api.cspace.add", data);
    }

    /**
     * 获取企业下的自定义空间
     * @param domain String	否	企业调用时传入，需要为10个字节以内的字符串，仅可包含字母和数字，大小写不敏感
     * @param agentId String	否	ISV调用时传入，微应用agentId
     * @return ApiResult
     */
    public ApiResult getCustomSpace(String domain, String agentId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("domain", domain)
                .put("agent_id", agentId)
                .getData();
        return get("api.cspace.get_custom_space", data);
    }

    /**
     * 授权用户访问企业下的自定义空间
     *
     * @param agentId	String	否	ISV调用时传入，授权访问指定微应用的自定义空间
     * @param domain	String	否	企业调用时传入，授权访问该domain的自定义空间
     * @param type	String	是	权限类型，目前支持上传和下载，上传请传add，下载请传download
     * @param userId	String	是	企业用户userid
     * @param path	String	否	授权访问的路径，如授权访问所有文件传"/"，授权访问/doc文件夹传"/doc/"
     * @param fileids	Boolean	否	授权访问的文件id列表，id之间用英文逗号隔开，如"fileId1,fileId2"
     * @param duration	Integer	是	权限有效时间，有效范围为0~3600秒，超出此范围或不传默认为30秒
     *
     * @return ApiResult
     */
    public ApiResult grantCustomSpace(String agentId, String domain, String type, String userId, String path,
                                      Boolean fileids, int duration) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("agent_id", agentId)
                .put("domain", domain)
                .put("type", type)
                .put("userid", userId)
                .put("path", path)
                .put("fileids", fileids)
                .put("duration", duration)
                .getData();
        return get("api.cspace.grant_custom_space", data);
    }


}
