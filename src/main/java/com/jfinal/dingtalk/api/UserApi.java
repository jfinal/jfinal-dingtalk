package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.List;
import java.util.Map;

/**
 * 用户相关api
 * Created by L.cm on 2016/6/3.
 */
public class UserApi extends BaseApi {

    /**
     * 根据unionid获取成员的userid
     * @param unionId	String	是	用户在当前钉钉开放平台账号范围内的唯一标识，同一个钉钉开放平台账号可以包含多个开放应用，同时也包含ISV的套件应用及企业应用
     * @return ApiResult
     */
    public ApiResult getUserIdByUnionId(String unionId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("unionid", unionId)
                .getData();
        return get("api.user.getUseridByUnionid", data);
    }

    /**
     * 获取成员详情
     * @param userId String	是	员工在企业内的UserID，企业用来唯一标识用户的字段。
     * @return ApiResult
     */
    public ApiResult get(String userId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("userid", userId)
                .put("lang", "zh_CN")
                .getData();
        return get("api.user.get", data);
    }

    /**
     * 创建成员
     * @param data 请求包结构体
     * @return ApiResult
     */
    public ApiResult create(Map<String, Object> data) {
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.user.create", data);
    }

    /**
     * 更新成员
     * @param data 请求包结构体
     * @return ApiResult
     */
    public ApiResult update(Map<String, Object> data) {
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.user.update", data);
    }

    /**
     * 删除成员
     * @param userId	String	是	员工唯一标识ID（不可修改）
     * @return ApiResult
     */
    public ApiResult delete(String userId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("userid", userId)
                .getData();
        return get("api.user.delete", data);
    }

    /**
     * 批量删除成员
     * @param userIdList List	是	员工UserID列表。列表长度在1到20之间
     * @return ApiResult
     */
    public ApiResult batchdelete(List<String> userIdList) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("useridlist", userIdList)
                .getData();
        return post("api.user.batchdelete", data);
    }

    /**
     * 获取部门成员
     * @param departmentId	long	是	获取的部门id
     * @param offset	long	否	支持分页查询，与size参数同时设置时才生效，此参数代表偏移量
     * @param size	int	否	支持分页查询，与offset参数同时设置时才生效，此参数代表分页大小，最大100
     * @param order	String	否	支持分页查询，部门成员的排序规则，默认不传是按自定义排序；entry_asc代表按照进入部门的时间升序，entry_desc代表按照进入部门的时间降序，modify_asc代表按照部门信息修改时间升序，modify_desc代表按照部门信息修改时间降序，custom代表用户定义(未定义时按照拼音)排序
     * @return ApiResult
     */
    public ApiResult simpleList(long departmentId, Long offset, Integer size, String order) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("department_id", departmentId)
                .put("offset", offset)
                .put("size", size)
                .put("order", order)
                .getData();
        return get("api.user.simplelist", data);
    }

    /**
     * 获取部门成员
     * @param departmentId	long	是	获取的部门id
     * @param offset	long	否	支持分页查询，与size参数同时设置时才生效，此参数代表偏移量
     * @param size	int	否	支持分页查询，与offset参数同时设置时才生效，此参数代表分页大小，最大100
     * @param order	String	否	支持分页查询，部门成员的排序规则，默认不传是按自定义排序；entry_asc代表按照进入部门的时间升序，entry_desc代表按照进入部门的时间降序，modify_asc代表按照部门信息修改时间升序，modify_desc代表按照部门信息修改时间降序，custom代表用户定义(未定义时按照拼音)排序
     * @return ApiResult
     */
    public ApiResult list(long departmentId, Long offset, Integer size, String order) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("department_id", departmentId)
                .put("offset", offset)
                .put("size", size)
                .put("order", order)
                .getData();
        return get("api.user.list", data);
    }

    /**
     * 通过CODE换取用户身份
     * @param code	String	是	通过Oauth认证会给URL带上CODE
     * @return ApiResult
     */
    public ApiResult getUserInfo(String code) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("code", code)
                .getData();
        return get("api.user.getuserinfo", data);
    }

}
