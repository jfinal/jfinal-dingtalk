package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;
import com.jfinal.kit.StrKit;

import java.util.Map;

/**
 * 部门api接口
 * Created by L.cm on 2016/6/3.
 */
public class DepartmentApi extends BaseApi {

    /**
     * 获取部门列表
     * @return ApiResult
     */
    public ApiResult list() {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("lang", "zh_CN")
                .getData();
        return get("api.department.list", data);
    }

    /**
     * 获取部门详情
     * @param id 部门id
     * @return ApiResult
     */
    public ApiResult get(String id) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("id", id)
                .put("lang", "zh_CN")
                .getData();
        return get("api.department.get", data);
    }

    /**
     * 创建部门
     * @param name              String	是	部门名称。长度限制为1~64个字符
     * @param parentId          String	是	父部门id。根部门id为1
     * @param order             String	否	在父部门中的次序值。order值小的排序靠前
     * @param createDeptGroup   Boolean	否	是否创建一个关联此部门的企业群，默认为false
     * @return
     */
    public ApiResult create(String name, String parentId, String order, Boolean createDeptGroup) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("name", name)
                .put("lang", "zh_CN")
                .getData();

        if (StrKit.isBlank(parentId)) {
            data.put("parentid", "1");
        } else {
            data.put("parentid", parentId);
        }
        if (StrKit.notBlank(order)) {
            data.put("order", order);
        }
        if (null != createDeptGroup) {
            data.put("createDeptGroup", true);
        }
        return post("api.department.create", data);
    }


    /**
     * 获取部门详情
     * @param data 部门信息，
     *
     *          {
     *             "name": "钉钉事业部",
     *             "parentid": "1",
     *             "order": "1",
     *             "id": "1",
     *             "createDeptGroup": true,
     *             "autoAddUser": true,
     *             "deptManagerUseridList": "manager1111|2222",
     *             "deptHiding" : true,
     *             "deptPerimits" : "3|4",
     *             "userPerimits" : "userid1|userid2",
     *             "outerDept" : true,
     *             "outerPermitDepts" : "1|2",
     *             "outerPermitUsers" : "userid3|userid4",
     *             "orgDeptOwner": "manager1111"
     *           }
     * @return ApiResult
     */
    public ApiResult update(Map<String, Object> data) {
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.department.update", data);
    }

    /**
     * 删除部门
     * @param id long	是	部门id。（注：不能删除根部门；不能删除含有子部门、成员的部门）
     * @return ApiResult
     */
    public ApiResult delete(long id) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("id", id)
                .getData();

        return get("api.department.delete", data);
    }

}
