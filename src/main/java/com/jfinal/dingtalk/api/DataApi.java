package com.jfinal.dingtalk.api;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class DataApi extends BaseApi {
    /**
     * 记录统计数据
     *
     * @param data
     * @return
     */
    public ApiResult record(Map<String, Object> data){
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.data.record", data);
    }

    /**
     * 记录统计数据
     *
     * @param data
     * @return
     */
    public ApiResult update(Map<String, Object> data){
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.data.update", data);
    }

}
