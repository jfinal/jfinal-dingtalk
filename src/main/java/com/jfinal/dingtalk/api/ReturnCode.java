package com.jfinal.dingtalk.api;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

/**
 * 全局返回码说明
 * http://g.alicdn.com/dingding/opendoc/docs/_server/tab15.html?t=1464836001108
 */
public class ReturnCode {
    private static final String PROP_FILE = "api/dingtalk_api_code.properties";

    /**
     * 通过返回码获取返回信息
     * @param errCode 错误码
     * @return {String}
     */
    public static String get(int errCode){
        Prop prop = PropKit.use(PROP_FILE);
        String result = prop.get(String.valueOf(errCode));
        return result != null ? result : "未知返回码：" + errCode;
    }

}
