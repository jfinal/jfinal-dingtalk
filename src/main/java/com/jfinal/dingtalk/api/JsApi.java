package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class JsApi extends BaseApi {

    /**
     * 通过CODE换取微应用管理员的身份信息
     *  type	String	是	这里是固定值，jsapi
     * @return ApiResult
     */
    public ApiResult getJsapiTicket() {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("type", "jsapi")
                .getData();
        return get("api.get_jsapi_ticket", data);
    }

}
