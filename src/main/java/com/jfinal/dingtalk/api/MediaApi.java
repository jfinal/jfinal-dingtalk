package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class MediaApi extends BaseApi {

    /**
     * 上传媒体文件
     * @param type	String	是	媒体文件类型，分别有图片（image）、语音（voice）、普通文件(file)
     * @param media	String	是	form-data中媒体文件标识，有filename、filelength、content-type等信息
     * @return ApiResult
     */
    public ApiResult upload(String type, String media) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("type", type)
                .put("media", media)
                .getData();

        // TODO add upload
        // https://github.com/ddtalk/HarleyCorp/blob/master/src/com/alibaba/dingtalk/openapi/demo/utils/HttpHelper.java
        return post("api.media.upload", data);
    }

    /**
     * 获取媒体文件
     * @param media_id
     * @return
     */
    public ApiResult get(String media_id) {
        return null;
    }
}
