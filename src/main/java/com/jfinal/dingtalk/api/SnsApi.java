package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class SnsApi extends BaseApi {

    /**
     * 获取用户授权的SNS_TOKEN
     * @param openId	String	是	用户的openid
     * @param persistentCode	String	是	用户授权给钉钉开放应用的持久授权码
     * @return ApiResult
     */
    public ApiResult getSNSToken(String openId, String persistentCode) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getSNSToken())
                .put("openid", openId)
                .put("persistent_code", persistentCode)
                .getData();
        return post("api.sns.get_sns_token", data);
    }

    /**
     * 获取用户授权的持久授权码
     * @param tmpAuthCode	String	是	用户授权给钉钉开放应用的临时授权码
     * @return ApiResult
     */
    public ApiResult getPersistentCode(String tmpAuthCode) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getSNSToken())
                .put("tmp_auth_code", tmpAuthCode)
                .getData();
        return post("api.sns.get_persistent_code", data);
    }

    /**
     * 获取用户授权的SNS_TOKEN
     * @param openId	String	是	用户的openid
     * @param persistentCode	String	是	用户授权给钉钉开放应用的持久授权码
     * @return ApiResult
     */
    public ApiResult getSnsToken(String openId, String persistentCode) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getSNSToken())
                .put("openid", openId)
                .put("persistent_code", persistentCode)
                .getData();
        return post("api.sns.get_sns_token", data);
    }

    /**
     * 获取用户授权的个人信息
     * @param snsToken	String	是	用户授权给开放应用的token
     * @return ApiResult
     */
    public ApiResult getUserInfo(String snsToken) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getSNSToken())
                .put("sns_token", snsToken)
                .getData();
        return get("api.sns.getuserinfo", data);
    }

}
