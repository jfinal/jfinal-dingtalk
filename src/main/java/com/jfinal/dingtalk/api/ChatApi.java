package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.utils.ParaMap;

import java.util.List;
import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class ChatApi extends BaseApi {

    /**
     * 创建会话
     * @param name	String	是	群名称。长度限制为1~20个字符
     * @param owner	String	是	群主userId，员工唯一标识ID；必须为该会话useridlist的成员之一
     * @param userIdList	String[]	是	群成员列表，每次最多操作40人，群人数上限为1000
     * @return ApiResult
     */
    public ApiResult create(String name, String owner, List<String> userIdList) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("name", name)
                .put("owner", owner)
                .put("useridlist", userIdList)
                .getData();
        return post("api.chat.create", data);
    }

    /**
     * 修改会话
     * @param chatId	String	是	群会话的id
     * @param name	String	是	群名称。长度限制为1~20个字符
     * @param owner	String	是	群主userId，员工唯一标识ID；必须为该会话useridlist的成员之一
     * @param addUserIdlist	String[]	否	添加成员列表，每次最多操作40人，群人数上限为1000
     * @param delUserIdlist	String[]	否	删除成员列表，每次最多操作40人，群人数上限为1000
     * @return ApiResult
     */
    public ApiResult update(String chatId, String name, String owner,
                            List<String> addUserIdlist, List<String> delUserIdlist) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("chatid", chatId)
                .put("name", name)
                .put("owner", owner)
                .put("add_useridlist", addUserIdlist)
                .put("del_useridlist", delUserIdlist)
                .getData();
        return post("api.chat.update", data);
    }

    /**
     * 获取会话
     * @param chatId	String	是	群会话的id
     * @return ApiResult
     */
    public ApiResult get(String chatId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("chatid", chatId)
                .getData();
        return get("api.chat.get", data);
    }

    /**
     * 绑定微应用和群会话
     * @param chatId	String	是	群会话的id
     * @param agentId	String	是	微应用agentId，每个群最多绑定5个微应用，一个群只能被一个ISV套件绑定一次
     * @return ApiResult
     */
    public ApiResult bind(String chatId, String agentId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("chatid", chatId)
                .put("agentid", agentId)
                .getData();
        return get("api.chat.bind", data);
    }

    /**
     * 解绑微应用和群会话
     * @param chatId	String	是	群会话的id
     * @param agentId	String	是	微应用agentId，每个群最多绑定5个微应用，一个群只能被一个ISV套件绑定一次
     * @return ApiResult
     */
    public ApiResult unbind(String chatId, String agentId) {
        Map<String, Object> data = ParaMap
                .create("access_token", AccessTokenApi.getAccessToken())
                .put("chatid", chatId)
                .put("agentid", agentId)
                .getData();
        return get("api.chat.unbind", data);
    }

    /**
     * 发送消息到群会话
     *
     * 详细文档：http://g.alicdn.com/dingding/opendoc/docs/_server/tab4.html?t=1466603207754#发送消息到群会话
     *
     * @param data
     * @return
     */
    public ApiResult send(Map<String, Object> data){
        data.put("access_token", AccessTokenApi.getAccessToken());
        return post("api.chat.send", data);
    }


}
