package com.jfinal.dingtalk.api;

import com.jfinal.dingtalk.cache.IAccessTokenCache;
import com.jfinal.dingtalk.common.JFinalDingtalkConfig;
import com.jfinal.dingtalk.common.JFinalDingtalkKit;
import com.jfinal.dingtalk.utils.ParaMap;

import java.util.Map;

/**
 * Created by L.cm on 2016/6/3.
 */
public class AccessTokenApi extends BaseApi {
    private static final long serialVersionUID = -8116701145056475330L;

    /**
     * 直接获取accessToken
     * @return String
     */
    public static String getAccessToken() {
        AccessToken accessToken = getToken();
        return accessToken.getAccessToken();
    }



    /**
     * 获取 AccessToken
     * @return AccessToken
     */
    private static synchronized AccessToken getToken() {
        JFinalDingtalkConfig config = JFinalDingtalkKit.getApiConfig();
        IAccessTokenCache cache = JFinalDingtalkKit.getAccessTokenCache();

        String corpId = config.getCorpId();

        // 拼接缓存的key，用户区分accesstoken和sso token
        String cacheKey = "access_token:" + corpId;

        AccessToken accessToken = cache.get(cacheKey);
        // 此处待修改
        if (null != accessToken && accessToken.isAvailable()) {
            return accessToken;
        }
        Map<String, Object> data = ParaMap
                .create("corpid", config.getCorpId())
                .put("corpsecret", config.getCorpSecret())
                .getData();

        accessToken = getAccessToken("api.gettoken", data);

        cache.set(cacheKey, accessToken);
        return accessToken;
    }

    public static void refreshAccessToken() {
        getToken();
    }

    /**
     * 获取微应用后台管理免登SsoToken
     * @return 获取到的凭证
     */
    public static String getSSOToken() {
        AccessToken accessToken = _getSSOToken();
        return accessToken.getAccessToken();
    }

    /**
     * 获取 AccessToken
     * @return AccessToken
     */
    private static synchronized AccessToken _getSSOToken() {
        JFinalDingtalkConfig config = JFinalDingtalkKit.getApiConfig();
        IAccessTokenCache cache = JFinalDingtalkKit.getAccessTokenCache();

        String corpId = config.getCorpId();

        // 拼接缓存的key，用户区分accesstoken和sso token
        String cacheKey = "sso_token:" + corpId;

        AccessToken accessToken = cache.get(cacheKey);
        // 此处待修改
        if (null != accessToken && accessToken.isAvailable()) {
            return accessToken;
        }
        Map<String, Object> data = ParaMap
                .create("corpid", config.getCorpId())
                .put("corpsecret", config.getSsoSecret())
                .getData();

        accessToken = getAccessToken("api.sso.gettoken", data);

        cache.set(cacheKey, accessToken);
        return accessToken;
    }


    /**
     * 获取微应用后台管理免登SsoToken
     * @return 获取到的凭证
     */
    public static String getSNSToken() {
        AccessToken accessToken = _getSNSToken();
        return accessToken.getAccessToken();
    }

    /**
     * 获取 AccessToken
     * @return AccessToken
     */
    private static synchronized AccessToken _getSNSToken() {
        JFinalDingtalkConfig config = JFinalDingtalkKit.getApiConfig();
        IAccessTokenCache cache = JFinalDingtalkKit.getAccessTokenCache();

        String corpId = config.getCorpId();

        // 拼接缓存的key，用户区分accesstoken和sns token
        String cacheKey = "sns_token:" + corpId;

        AccessToken accessToken = cache.get(cacheKey);
        // 此处待修改
        if (null != accessToken && accessToken.isAvailable()) {
            return accessToken;
        }
        Map<String, Object> data = ParaMap
                .create("corpid", config.getCorpId())
                .put("corpsecret", config.getSsoSecret())
                .getData();

        accessToken = getAccessToken("api.sns.gettoken", data);

        cache.set(cacheKey, accessToken);
        return accessToken;
    }
}
