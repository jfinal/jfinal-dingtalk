package com.jfinal.dingtalk.common;

import java.io.Serializable;

public class JFinalDingtalkConfig implements Serializable {
    private static final long serialVersionUID = 2641001803290623919L;

    private String corpId;
    private String corpSecret;
    private String ssoSecret;

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getCorpSecret() {
        return corpSecret;
    }

    public void setCorpSecret(String corpSecret) {
        this.corpSecret = corpSecret;
    }

    public String getSsoSecret() {
        return ssoSecret;
    }

    public void setSsoSecret(String ssoSecret) {
        this.ssoSecret = ssoSecret;
    }
}
