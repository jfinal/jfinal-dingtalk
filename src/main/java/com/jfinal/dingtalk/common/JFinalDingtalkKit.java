package com.jfinal.dingtalk.common;

import com.jfinal.dingtalk.cache.DefaultAccessTokenCache;
import com.jfinal.dingtalk.cache.IAccessTokenCache;

/**
 * Created by L.cm on 2016/6/6.
 */
public class JFinalDingtalkKit {
    private static final ThreadLocal<JFinalDingtalkConfig> tl = new ThreadLocal<JFinalDingtalkConfig>();

    // 开发模式将输出请求信息
    private static boolean devMode = false;

    public static void setDevMode(boolean devMode) {
        JFinalDingtalkKit.devMode = devMode;
    }

    public static boolean isDevMode() {
        return devMode;
    }

    public static void setThreadLocalApiConfig(JFinalDingtalkConfig apiConfig) {
        tl.set(apiConfig);
    }

    public static void removeThreadLocalApiConfig() {
        tl.remove();
    }

    public static JFinalDingtalkConfig getApiConfig() {
        JFinalDingtalkConfig result = tl.get();
        if (result == null)
            throw new IllegalStateException("需要事先使用 JFinalDingtalkKit.setThreadLocalApiConfig(apiConfig) 将 ApiConfig对象存入，才可以调用 ApiConfigKit.getApiConfig() 方法");
        return result;
    }

    static IAccessTokenCache accessTokenCache = new DefaultAccessTokenCache();

    public static void setAccessTokenCache(IAccessTokenCache accessTokenCache) {
        JFinalDingtalkKit.accessTokenCache = accessTokenCache;
    }

    public static IAccessTokenCache getAccessTokenCache() {
        return JFinalDingtalkKit.accessTokenCache;
    }
}
